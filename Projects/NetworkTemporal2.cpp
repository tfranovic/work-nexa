#include <string>
#include <sstream>
#include <iostream>
#include <fstream>

#include "NetworkBCPNN.h"
#include "NetworkKussul.h"
#include "NetworkCL.h"
#include "NetworkMDS.h"
#include "NetworkMI.h"
#include "NetworkVQ.h"
#include "Network.h"

#include "NetworkTemporal2.h"
#include "NetworkTriesch.h"
#include "NetworkFoldiak.h"

//using namespace std;

void NetworkTemporal2::NetworkInitParameters() {
	//m_dataAssigner=new SequentialAssignmentStrategy;
	m_dataAssigner=new SequentialAssignmentStrategy();
	m_architecture = PC;
	// layer 1
	// 768 pixels in each data item
	m_nrInputHypercolumns = 12;//768;//720;//784;//784;//50;//784;
	m_nrInputRateUnits = 12;//784;//nrColors;

	// layer 2
	m_nrHypercolumns2 = 12;//48;			// number clusters/patches
	m_nrRateUnits2 = 12;//100;//200;	// number code vectors/units in each cluster

	// layer 3
	m_nrHypercolumns3 = 1;//9;		// 2nd layer cluster
	m_nrRateUnits3 = m_nrHypercolumns2*m_nrRateUnits2;//20;	// 2nd layer code vectors

	m_MDSInput = 20;
	m_MDS2 = 20;
}

void NetworkTemporal2::NetworkSetupStructure()
{
	

//	int nrPatches = 10;			// nr patches
	//m_sizePopulation2 = 100;	// nr code vectors per patch

	// Structures setting up everything to calculate mutual information (MI)/correlations (Pearson) on input data + multi-dimensional scaling (MDS) + vector quantization (VQ)

	m_layerInput = new PopulationColumns(this,m_nrInputHypercolumns,m_nrInputRateUnits,PopulationColumns::Graded,MPIDistribution::ParallelizationHypercolumns); // input
	m_layer3 = new PopulationColumns(this,m_nrHypercolumns3,m_nrRateUnits3,PopulationColumns::Graded,MPIDistribution::ParallelizationHypercolumns);//m_structureLayer2->GetLayer(1);
	WTA* w = new WTA();
	m_layer3->AddLayerEvent(w);
	AddLayer(m_layerInput);
	AddLayer(m_layer3);

	FullConnectivity* full10 = new FullConnectivity();
	m_layer3->AddPre(m_layerInput,full10); // Feedforward
	bClassification = new ConnectionModifierBcpnnOnline(0.0005, 10e-6);
	full10->AddConnectionsEvent(bClassification);

	//m_structureInput->MDSHypercolumns()->SetUseThreshold(true,0.9);
	bClassification->SwitchOnOff(false);

	AddTiming(this);
	AddTiming(m_layerInput);
	AddTiming(m_layer3);
}

void NetworkTemporal2::NetworkSetupMeters()
{
	m_activityMeter = new Meter("ClassOutputBaseline.csv",Storage::CSV);
	m_activityMeter->AttachLayer(m_layer3);
	m_activityMeter->SwitchOnOff(false);
	AddMeter(m_activityMeter);
}

void NetworkTemporal2::NetworkSetupParameters()
{
	// not used
}

vector<float> NetworkTemporal2::toBinary(vector<float> data, int nrHc, int nrMc)
{
	vector<float> out(nrHc*nrMc);

	int currentI = 0;
	for(int i=0;i<data.size();i++)
	{
		out[currentI+data[i]] = 1.0;
		currentI+=nrMc;		
	}

	return out;
}

void NetworkTemporal2::ComputeCorrelation(vector<vector<float> > trainingData,  PopulationColumns* inputLayer, StructureMIMDSVQ* structure, int iter) {
	structure->Pearson()->SwitchOnOff(true);
	for(int i=0;i<trainingData.size();i++) {
		if(this->MPIGetNodeId() == 0) {
			cout<<i<<"("<<iter<<") ";
			cout.flush();
		}
		inputLayer->SetValuesAll(m_dataAssigner->prepareValues(i,trainingData));
		this->Simulate();
		cout.flush();
	}
	if(this->MPIGetNodeId() == 0)
		cout<<"\n";
	structure->Pearson()->SwitchOnOff(false);
}

void NetworkTemporal2::ComputeMDS(vector<vector<float> > trainingData,  PopulationColumns* inputLayer, StructureMIMDSVQ* structure, int iter) {
	structure->MDSHypercolumns()->SwitchOnOff(true);
	structure->MDS()->SwitchOnOff(true);
	for(int i=0;i<trainingData.size();i++) {
		if(this->MPIGetNodeId() == 0) {
			cout<<i<<"("<<iter<<") ";
			cout.flush();
		}
		inputLayer->SetValuesAll(m_dataAssigner->prepareValues(i,trainingData));
		this->Simulate();
		cout.flush();
	}
	if(this->MPIGetNodeId() == 0)
		cout<<"\n";
	structure->MDSHypercolumns()->SwitchOnOff(false);
	structure->MDS()->SwitchOnOff(false);
}

void NetworkTemporal2::ComputeVQ(vector<vector<float> > trainingData,  PopulationColumns* inputLayer, StructureMIMDSVQ* structure, int iter) {
	structure->GetLayer(1)->SwitchOnOff(true);
	structure->VQ()->SwitchOnOff(true);
	for(int i=0;i<trainingData.size();i++) {
		if(this->MPIGetNodeId() == 0) {
			cout<<i<<"("<<iter<<") ";
			cout.flush();
		}
		inputLayer->SetValuesAll(m_dataAssigner->prepareValues(i,trainingData));
		this->Simulate();
		cout.flush();
	}
	if(this->MPIGetNodeId() == 0)
		cout<<"\n";
	structure->VQ()->SwitchOnOff(false);
}

void NetworkTemporal2::ExtractFeatures(vector<vector<float> > trainingData,  PopulationColumns* inputLayer, StructureMIMDSVQ* structure) {
	structure->CSLLearn()->SwitchOnOff(true);
	structure->SetRecording(false);
	float clC = m_nrRateUnits2*2;
	for(int i=0;i<trainingData.size();i++)
	{
		inputLayer->SetValuesAll(m_dataAssigner->prepareValues(i,trainingData));
		// next time step
		this->Simulate();
		cout.flush();
	}
	if(this->MPIGetNodeId()== 0)
		cout<<"\n";
		
	//this->RecordAll();
	structure->CSLLearn()->SwitchOnOff(false);
}

void NetworkTemporal2::TrainLayer(vector<vector<vector<float> > > trainingData, PopulationColumns* inputLayer, StructureMIMDSVQ* structure, int iterationsPatches, int iterationsFeatures)
{
	// Training phase
	int nrTrainImages = 0;
	// switch off for some timesteps
	structure->MDSHypercolumns()->SwitchOnOff(false);
	structure->MDS()->SwitchOnOff(false);
	structure->VQ()->SwitchOnOff(false);
	structure->CSLLearn()->SwitchOnOff(false);
	structure->GetLayer(1)->SwitchOnOff(false);

	structure->SetRecording(false);
	int j=0;
	for(;j<iterationsPatches;j++) {
		if(j==(int)iterationsPatches*0.8)
			break;
		for(int i=0;i<trainingData.size();i++) {
			ComputeCorrelation(trainingData[i],inputLayer,structure,j);
			if(j==0)
				nrTrainImages+=trainingData[i].size();
		}
	}

	for(;j<iterationsPatches;j++) {
		if(j==(int)iterationsPatches*0.9)
			break;
		for(int i=0;i<trainingData.size();i++) {
			ComputeMDS(trainingData[i],inputLayer,structure,j);
		}
	}

	structure->CSLLearn()->SetMaxPatterns(nrTrainImages);
	
	for(;j<iterationsPatches;j++) {
		for(int i=0;i<trainingData.size();i++) {
			ComputeVQ(trainingData[i],inputLayer,structure,j);
		}
	}

	for(int j=0;j<iterationsFeatures;j++) {
		for(int i=0;i<trainingData.size();i++) {
			ExtractFeatures(trainingData[i],inputLayer,structure);
		}
		if(j==iterationsFeatures-2)
			structure->SetRecording(true);
	}
	structure->CSLLearn()->SwitchOnOff(false);
	this->RecordAll();
}

void NetworkTemporal2::NetworkRun()
{
	int nrColors = 2;
	char* filenameTrain;
	char* filenameTest;
	char* filenameTrainLabels;
	char* filenameTestLabels;
	char* name;
	int nrTrainImages = 20;//2000;//1000;
	int nrTestImages = 100;

	int iterationsPatches = 10;//300;
	
	int iterationsFeatures = 1;
	vector<vector<float> > trainingData;
	vector<vector<float> > testingData;
	vector<vector<float> > trainingLabels;
	vector<vector<float> > testingLabels;

	if(m_architecture == PC)
	{
		nrTrainImages = 25;
		//iterationsPatches = 10;
		filenameTrain="D:\\Databases\\tidigits\\trainDataBCPNN.csv";
		filenameTrainLabels="D:\\Databases\\tidigits\\trainDataLabels.csv";
		filenameTest="D:\\Databases\\tidigits\\testDataBCPNN.csv";
		//filenameTestLabels="D:\\Databases\\tidigits\\testLabels.csv";
	}
	else {
		nrTrainImages = 25000;
		filenameTrain="/cfs/klemming/nobackup/p/paherman/Tin/data/trainDataBCPNN.csv";
		filenameTrainLabels="/cfs/klemming/nobackup/p/paherman/Tin/data/trainDataLabels.csv";
		filenameTest="/cfs/klemming/nobackup/p/paherman/Tin/data/testDataBCPNN.csv";
		filenameTestLabels="/cfs/klemming/nobackup/p/paherman/Tin/data/testDataLabels.csv";
	}
	int stepsStimuliOn = 1;

	// Specify input data
	Storage storage;
	Storage storageLabels;
	storage.SetMPIParameters(this->MPIGetNodeId(),this->MPIGetNrProcs());

	trainingData=storage.LoadDataFloatCSV(filenameTrain,nrTrainImages,true);
	testingData=storage.LoadDataFloatCSV(filenameTest,nrTrainImages,true);
	trainingLabels=storage.LoadDataFloatCSV(filenameTrainLabels,nrTrainImages,true);
	//testingLabels=storage.LoadDataFloatCSV(filenameTest,nrTrainImages,true);
	vector<int> partsOfDataToUseAsInput = m_layerInput->GetMPIDistributionHypercolumns(this->MPIGetNodeId());
	vector<int> partsOfDataToUseAsOutput = vector<int>();//layer1->GetMPIDistributionHypercolumns(mpiRank);

	m_layerInput->SwitchOnOff(false);
	m_layer3->SwitchOnOff(false);
	m_layer3->SetRecording(false);
	bClassification->SwitchOnOff(false);
	
	// Run through all training and test data
	TimingStart("RunThrough");
	int prevLabel=-1;
	for (int k=0;k<3;k++) {
		for(int j=0;j<trainingData.size();j++) {
			if(prevLabel<0 && j==0) {
				bClassification->SwitchOnOff(false);
				prevLabel=j;
				m_layerInput->SetValuesAll(m_dataAssigner->prepareValues(j,trainingData));
				this->Simulate();
				bClassification->SwitchOnOff(true);
			}
			vector<float> v(m_nrRateUnits3,0.0);
			v[prevLabel]=1.0;
			prevLabel=(int)trainingLabels[j][0]-1;
			m_layerInput->SetValuesAll(m_dataAssigner->prepareValues(j,trainingData));
			m_layer3->SetValuesAll(v);
			this->Simulate();
		}
		this->Simulate();
	}
	this->RecordAll();
	TimingStop("RunThrough");
	m_layer3->SwitchOnOff(true);
	bClassification->SwitchOnOff(false);
	for(int j=0;j<trainingData.size();j++) {
		if(j==1)
			m_layer3->SetRecording(true);
		m_layerInput->SetValuesAll(m_dataAssigner->prepareValues(j,trainingData));
		this->Simulate();
	}
	if(testingData.size()>0) {
		for(int j=0;j<testingData.size();j++) {
			m_layerInput->SetValuesAll(m_dataAssigner->prepareValues(j,testingData));
			this->Simulate();
		}
	}
	this->Simulate();
	this->RecordAll();
	// Train 2nd layer
	this->StoreAnalysis();
}

void NetworkTemporal2::SetInputLayerSize(int size) {
	m_nrInputHypercolumns=size;
}

void NetworkTemporal2::SetLayer2Size(int size) {
	m_nrHypercolumns2=size;
	m_nrHypercolumns3=size;
}

void NetworkTemporal2::SetMDSSize(int size) {
	m_MDSInput=size;
	m_MDS2=size;
}

string NetworkTemporal2::GetMDSInput() {
	return "";//m_structureInput->MDS()->GetStress();
}
