#include <string>
#include <sstream>
#include <iostream>
#include <fstream>

#include "NetworkBCPNN.h"
#include "NetworkKussul.h"
#include "NetworkCL.h"
#include "NetworkMDS.h"
#include "NetworkMI.h"
#include "NetworkVQ.h"
#include "Network.h"

#include "NetworkTemporal.h"
#include "NetworkTriesch.h"
#include "NetworkFoldiak.h"

//using namespace std;

void NetworkTemporal::NetworkInitParameters() {
	//m_dataAssigner=new SequentialAssignmentStrategy;
	m_dataAssigner=new SequentialAssignmentStrategy();
	m_architecture = PC;
	// layer 1
	// 768 pixels in each data item
	m_nrInputHypercolumns = 13;//768;//720;//784;//784;//50;//784;
	m_nrInputRateUnits = 33;//784;//nrColors;

	// layer 2
	m_nrHypercolumns2 = 12;//48;			// number clusters/patches
	m_nrRateUnits2 = 12;//100;//200;	// number code vectors/units in each cluster

	// layer 3
	m_nrHypercolumns3 = 1;//9;		// 2nd layer cluster
	m_nrRateUnits3 = m_nrHypercolumns2*m_nrRateUnits2;//20;	// 2nd layer code vectors

	m_MDSInput = 20;
	m_MDS2 = 20;
}

void NetworkTemporal::NetworkSetupStructure()
{
	

//	int nrPatches = 10;			// nr patches
	//m_sizePopulation2 = 100;	// nr code vectors per patch

	// Structures setting up everything to calculate mutual information (MI)/correlations (Pearson) on input data + multi-dimensional scaling (MDS) + vector quantization (VQ)
	m_structureInput = new StructureMIMDSVQ(); 
	m_structureLayer2 = new StructureMIMDSVQ();

	m_layerInput = new PopulationColumns(this,m_nrInputHypercolumns,m_nrInputRateUnits,PopulationColumns::Graded,MPIDistribution::ParallelizationHypercolumns); // input
	m_structureInput->SetMDSDimension(m_MDSInput); // Number of dimensions in multi-dimensional scaling matrix (Size input x dimensions)
	m_structureInput->SetMDSMeasure(true);
	m_structureLayer2->SetMDSDimension(m_MDS2);
	m_structureLayer2->SetMDSMeasure(true);
	
	m_structureInput->SetupStructure(this,m_layerInput,m_nrHypercolumns2,m_nrRateUnits2, true);
	m_layer2 = m_structureInput->GetLayer(1);

	m_structureLayer2->SetupStructure(this,m_layer2,m_nrHypercolumns3,m_nrRateUnits3, false);
	m_layer3 = new PopulationColumns(this,m_nrHypercolumns3,m_nrRateUnits3,PopulationColumns::Graded,MPIDistribution::ParallelizationHypercolumns);//m_structureLayer2->GetLayer(1);
	WTA* w = new WTA();
	m_layer3->AddLayerEvent(w);
	AddLayer(m_layer3);

	FullConnectivity* full10 = new FullConnectivity();
	m_layer3->AddPre(m_layer2,full10); // Feedforward
	bClassification = new ConnectionModifierBcpnnOnline(0.00001, 10e-6);
	full10->AddConnectionsEvent(bClassification);

	//m_structureInput->MDSHypercolumns()->SetUseThreshold(true,0.9);
	m_structureInput->VQ()->GetCSL()->SetEta(0.001,true);
	m_structureInput->MDSHypercolumns()->SetUpdateSize(2e-3);
	m_structureLayer2->VQ()->GetCSL()->SetEta(0.001,true);
	m_structureLayer2->MDSHypercolumns()->SetUpdateSize(2e-3);

	m_structureInput->MDSHypercolumns()->SwitchOnOff(false);
	m_structureInput->MDS()->SwitchOnOff(false);
	m_structureInput->VQ()->SwitchOnOff(false);
	m_structureInput->CSLLearn()->SwitchOnOff(false);
	m_structureLayer2->MDSHypercolumns()->SwitchOnOff(false);
	m_structureLayer2->MDS()->SwitchOnOff(false);
	m_structureLayer2->VQ()->SwitchOnOff(false);
	m_structureLayer2->CSLLearn()->SwitchOnOff(false);
	bClassification->SwitchOnOff(false);

	AddTiming(this);
	AddTiming(m_structureInput->VQ());
	AddTiming(m_structureInput->MDSHypercolumns());
	AddTiming(m_structureInput->MDS());
	AddTiming(m_layerInput);
	AddTiming(m_layer2);
	AddTiming(m_layer3);
	AddTiming(m_structureInput->CSLLearn());
	AddTiming(bClassification);
}

void NetworkTemporal::NetworkSetupMeters()
{
	m_structureInput->SetupMeters(this->MPIGetNodeId(),this->MPIGetNrProcs());
	m_structureLayer2->SetIndex(1);
	m_structureLayer2->SetupMeters(this->MPIGetNodeId(),this->MPIGetNrProcs());
	m_activityMeter = new Meter("ClassOutput.csv",Storage::CSV);
	m_activityMeter->AttachLayer(m_layer3);
	m_activityMeter->SwitchOnOff(false);
	AddMeter(m_activityMeter);
}

void NetworkTemporal::NetworkSetupParameters()
{
	// not used
}

vector<float> NetworkTemporal::toBinary(vector<float> data, int nrHc, int nrMc)
{
	vector<float> out(nrHc*nrMc);

	int currentI = 0;
	for(int i=0;i<data.size();i++)
	{
		out[currentI+data[i]] = 1.0;
		currentI+=nrMc;		
	}

	return out;
}

void NetworkTemporal::ComputeCorrelation(vector<vector<float> > trainingData,  PopulationColumns* inputLayer, StructureMIMDSVQ* structure, int iter) {
	structure->Pearson()->SwitchOnOff(true);
	for(int i=0;i<trainingData.size();i++) {
		if(this->MPIGetNodeId() == 0) {
			cout<<i<<"("<<iter<<") ";
			cout.flush();
		}
		inputLayer->SetValuesAll(m_dataAssigner->prepareValues(i,trainingData));
		this->Simulate();
		cout.flush();
	}
	if(this->MPIGetNodeId() == 0)
		cout<<"\n";
	structure->Pearson()->SwitchOnOff(false);
}

void NetworkTemporal::ComputeMDS(vector<vector<float> > trainingData,  PopulationColumns* inputLayer, StructureMIMDSVQ* structure, int iter) {
	structure->MDSHypercolumns()->SwitchOnOff(true);
	structure->MDS()->SwitchOnOff(true);
	for(int i=0;i<trainingData.size();i++) {
		if(this->MPIGetNodeId() == 0) {
			cout<<i<<"("<<iter<<") ";
			cout.flush();
		}
		inputLayer->SetValuesAll(m_dataAssigner->prepareValues(i,trainingData));
		this->Simulate();
		cout.flush();
	}
	if(this->MPIGetNodeId() == 0)
		cout<<"\n";
	structure->MDSHypercolumns()->SwitchOnOff(false);
	structure->MDS()->SwitchOnOff(false);
}

void NetworkTemporal::ComputeVQ(vector<vector<float> > trainingData,  PopulationColumns* inputLayer, StructureMIMDSVQ* structure, int iter) {
	structure->GetLayer(1)->SwitchOnOff(true);
	structure->VQ()->SwitchOnOff(true);
	for(int i=0;i<trainingData.size();i++) {
		if(this->MPIGetNodeId() == 0) {
			cout<<i<<"("<<iter<<") ";
			cout.flush();
		}
		inputLayer->SetValuesAll(m_dataAssigner->prepareValues(i,trainingData));
		this->Simulate();
		cout.flush();
	}
	if(this->MPIGetNodeId() == 0)
		cout<<"\n";
	structure->VQ()->SwitchOnOff(false);
}

void NetworkTemporal::ExtractFeatures(vector<vector<float> > trainingData,  PopulationColumns* inputLayer, StructureMIMDSVQ* structure) {
	structure->CSLLearn()->SwitchOnOff(true);
	structure->SetRecording(false);
	float clC = m_nrRateUnits2*2;
	for(int i=0;i<trainingData.size();i++)
	{
		inputLayer->SetValuesAll(m_dataAssigner->prepareValues(i,trainingData));
		// next time step
		this->Simulate();
		cout.flush();
	}
	if(this->MPIGetNodeId()== 0)
		cout<<"\n";
		
	//this->RecordAll();
	structure->CSLLearn()->SwitchOnOff(false);
}

void NetworkTemporal::TrainLayer(vector<vector<float> > trainingData, PopulationColumns* inputLayer, StructureMIMDSVQ* structure, int iterationsCorrs, int iterationsMDS, int iterationsVQ, int iterationsFeatures)
{
	// Training phase
	int nrTrainImages = 0;
	// switch off for some timesteps
	structure->MDSHypercolumns()->SwitchOnOff(false);
	structure->MDS()->SwitchOnOff(false);
	structure->VQ()->SwitchOnOff(false);
	structure->CSLLearn()->SwitchOnOff(false);
	structure->GetLayer(1)->SwitchOnOff(false);

	structure->SetRecording(false);
	nrTrainImages=trainingData.size();

	int j=0;

	while(j<iterationsCorrs) {
		//if(j==(int)iterationsPatches*0.8)
		//	break;
		//for(int i=0;i<trainingData.size();i++) {
		ComputeCorrelation(trainingData,inputLayer,structure,j);
		j++;
	}
	if(this->MPIGetNodeId() == 0) {
		FILE *f=fopen("corr.csv","w+");
		for(int i=0;i<structure->Pearson()->rIJ.size();i++) {
			for(int k=0;k<structure->Pearson()->rIJ[i].size();k++) {
				if(k==0)
					fprintf(f,"%f",structure->Pearson()->rIJ[i][k]);
				else
					fprintf(f,",%f",structure->Pearson()->rIJ[i][k]);
			}
			if(i<structure->Pearson()->rIJ.size()-1)
				fprintf(f,"\n");
		}
	}
	
	j=0;
	while(j<iterationsMDS) {
		//if(j==(int)iterationsPatches*0.9)
		//	break;
		//for(int i=0;i<trainingData.size();i++) {
		ComputeMDS(trainingData,inputLayer,structure,j);
		j++;
	}
	j=0;
	while(j<iterationsVQ) {
		//for(int i=0;i<trainingData.size();i++) {
			if(this->MPIGetNodeId() == 0)
				cout << "DataPoint (patches): " << j <<endl;
		ComputeVQ(trainingData,inputLayer,structure,j);
		j++;
	}

	structure->CSLLearn()->SetMaxPatterns(nrTrainImages);
	j=0;
	while(j<iterationsFeatures) {
		//for(int i=0;i<trainingData.size();i++) {
			if(this->MPIGetNodeId() == 0)
				cout << "DataPoint (features): " << j <<endl;
			ExtractFeatures(trainingData,inputLayer,structure);
		//}
			j++;
	}
	structure->CSLLearn()->SwitchOnOff(false);
	this->RecordAll();
}

void NetworkTemporal::NetworkRun()
{
	int nrColors = 2;
	char* filenameTrain;
	char* filenameTest;
	char* filenameTrainLabels;
	char* filenameTestLabels;
	char* name;
	int nrTrainImages = 20;//2000;//1000;
	int nrTestImages = 100;

	int iterationsPatches = 10;//300;
	
	int iterationsFeatures = 1;
	vector<vector<float> > trainingData;
	vector<vector<float> > testingData;
	vector<vector<float> > trainingLabels;
	vector<vector<float> > testingLabels;

	if(m_architecture == PC)
	{
		nrTrainImages = 20;
		//iterationsPatches = 10;
		filenameTrain="D:\\Databases\\tidigits\\trainData.csv";
		filenameTrainLabels="D:\\Databases\\tidigits\\trainDataLabels.csv";
		filenameTest="D:\\Databases\\tidigits\\trainData.csv";
		//filenameTestLabels="D:\\Databases\\tidigits\\testLabels.csv";
	}
	else {
		nrTrainImages = 25000;
		filenameTrain="/cfs/klemming/nobackup/p/paherman/Tin/data/trainData.csv";
		filenameTrainLabels="/cfs/klemming/nobackup/p/paherman/Tin/data/trainDataLabels.csv";
		filenameTest="/cfs/klemming/nobackup/p/paherman/Tin/data/testData.csv";
		filenameTestLabels="/cfs/klemming/nobackup/p/paherman/Tin/data/testDataLabels.csv";
	}
	int stepsStimuliOn = 1;

	// Specify input data
	Storage storage;
	Storage storageLabels;
	storage.SetMPIParameters(this->MPIGetNodeId(),this->MPIGetNrProcs());

	trainingData=storage.LoadDataFloatCSV(filenameTrain,nrTrainImages,true);
	testingData=storage.LoadDataFloatCSV(filenameTest,nrTrainImages,true);
	trainingLabels=storage.LoadDataFloatCSV(filenameTrainLabels,nrTrainImages,true);
	//testingLabels=storage.LoadDataFloatCSV(filenameTest,nrTrainImages,true);

	vector<int> partsOfDataToUseAsInput = m_layerInput->GetMPIDistributionHypercolumns(this->MPIGetNodeId());
	vector<int> partsOfDataToUseAsOutput = vector<int>();//layer1->GetMPIDistributionHypercolumns(mpiRank);

	m_layerInput->SwitchOnOff(false);
	m_structureInput->CSLLearn()->SetMaxPatterns(trainingData.size());
	m_structureLayer2->CSLLearn()->SetMaxPatterns(trainingData.size());
	m_structureInput->SetRecording(false);
	m_structureLayer2->SetRecording(false);
	m_layer3->SwitchOnOff(false);
	m_layer3->SetRecording(false);
	bClassification->SwitchOnOff(false);
	// Train 1st layer
	m_layer2->GetIncomingConnections()[1]->SwitchOnOff(false); // turn off the connections calculating correlations in layer 2 so (change to switch off in structure by default instead) (!)
	m_layer2->GetIncomingConnections()[2]->SwitchOnOff(false);

	int totalNrTrainingData = trainingData.size();

	// Number of train iterations for each algorithm part
	int iterationsCorrs = 5; // run through x times, 5
	int iterationsMDS = 2;//200
	int iterationsVQ = 1;//200
	iterationsFeatures = 1; //+nrtraindata the timesteps to run CSL (first totalNrTrainingData steps are to collect the data to run on)

	TrainLayer(trainingData,m_layerInput,m_structureInput,iterationsCorrs,iterationsMDS,iterationsVQ,iterationsFeatures);
	
	// Run through all training and test data
	m_layer2->SwitchOnOff(true);
	m_layer2->SetRecording(false);
	TimingStart("RunThrough");
	int prevLabel=-1;
	for (int k=0;k<3;k++) {
		for(int j=0;j<trainingData.size();j++) {
			if(prevLabel<0&&j==0) {
				bClassification->SwitchOnOff(false);
				prevLabel=j;
				m_layerInput->SetValuesAll(m_dataAssigner->prepareValues(j,trainingData));
				this->Simulate();
				bClassification->SwitchOnOff(true);
			}
			if(k==2 && j==1)
				m_layer2->SetRecording(true);
			vector<float> v(m_nrRateUnits3,0.0);
			v[prevLabel]=1.0;
			prevLabel=(int)trainingLabels[j][0]-1;
			m_layerInput->SetValuesAll(m_dataAssigner->prepareValues(j,trainingData));
			m_layer3->SetValuesAll(v);
			this->Simulate();
		}
		this->Simulate();
	}
	this->RecordAll();
	TimingStop("RunThrough");
	m_layer3->SwitchOnOff(true);
	bClassification->SwitchOnOff(false);
	for(int j=0;j<trainingData.size();j++) {
		if(j==2)
			m_layer3->SetRecording(true);
		m_layerInput->SetValuesAll(m_dataAssigner->prepareValues(j,trainingData));
		this->Simulate();
	}
	if(testingData.size()>0) {
		for(int j=0;j<testingData.size();j++) {
			m_layerInput->SetValuesAll(m_dataAssigner->prepareValues(j,testingData));
			this->Simulate();
		}
	}
	m_layer2->SetRecording(false);
	this->Simulate();
	this->Simulate();
	this->RecordAll();
	// Train 2nd layer
	this->StoreAnalysis();
}

void NetworkTemporal::SetInputLayerSize(int size) {
	m_nrInputHypercolumns=size;
}

void NetworkTemporal::SetLayer2Size(int size) {
	m_nrHypercolumns2=size;
	m_nrHypercolumns3=size;
}

void NetworkTemporal::SetMDSSize(int size) {
	m_MDSInput=size;
	m_MDS2=size;
}

string NetworkTemporal::GetMDSInput() {
	return m_structureInput->MDS()->GetStress();
}
