#include "Network.h"

class NetworkDemoVis0 : public Network
{
public:

	
private:

	void NetworkSetupStructure();
	void NetworkSetupMeters();
	void NetworkSetupParameters();
	void NetworkRun();
};