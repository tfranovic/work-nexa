#pragma once


#include <iostream>
#include <vector>

#include "StructureMIMDSVQ.h"
#include "AssignmentStrategy.h"

class Network;
class PopulationColumns;

using namespace std;


class NetworkTemporal : public Network
{
public:
	// setters
	void SetInputLayerSize(int size);
	void SetLayer2Size(int size);
	void SetMDSSize(int size);

	// getters

	string GetMDSInput();

private:
	// overridden network functions
	void NetworkSetupStructure();
	void NetworkInitParameters();
	void NetworkSetupMeters();
	void NetworkSetupParameters();
	void NetworkRun();

	// training process functions
	void ComputeCorrelation(vector<vector<float> > trainingData,  PopulationColumns* inputLayer, StructureMIMDSVQ* structure, int iter);
	void ComputeMDS(vector<vector<float> > trainingData,  PopulationColumns* inputLayer, StructureMIMDSVQ* structure, int iter);
	void ComputeVQ(vector<vector<float> > trainingData,  PopulationColumns* inputLayer, StructureMIMDSVQ* structure, int iter);
	void ExtractFeatures(vector<vector<float> > trainingData,  PopulationColumns* inputLayer, StructureMIMDSVQ* structure);
	
	// Train
	void TrainLayer(vector<vector<float> > trainingData, PopulationColumns* inputLayer, StructureMIMDSVQ* structure, int iterationsCorrs, int iterationsMDS, int iterationsVQ, int iterationsFeatures);

	// other
	vector<float> toBinary(vector<float> data, int nrHc, int nrMc);
	//void ClearActivities();

	


	// Run
	bool m_run;
	int m_architecture;

	// Populations
	PopulationColumns* m_layerInput;
	PopulationColumns* m_layer2, *m_layer3;

	// Plasticity
	ConnectionModifierBcpnnOnline* m_bcpnn;

	// Pre-defined structures
	//vector<StructureReTIDe*> m_reTIDe;

	// Assignment strategy
	IAssignmentStrategy* m_dataAssigner;
	
	// Sizes
	int m_sizePopulation1, m_sizePopulation2, m_sizePopulation3;
	int m_nrHypercolumns2, m_nrRateUnits2, m_nrHypercolumns3, m_nrRateUnits3;
	int m_nrInputHypercolumns, m_nrInputRateUnits;
	int m_MDSInput, m_MDS2;

	int m_nrColumns;

	// Structures
	StructureMIMDSVQ* m_structureInput, *m_structureLayer2;
	Meter* m_activityMeter;
	ConnectionModifierBcpnnOnline* bClassification;
	// Probabilities
	//float m_probRecurr, m_probRecurr2, m_probForward;

	// Strengths
	float m_plasticityStrength;
};