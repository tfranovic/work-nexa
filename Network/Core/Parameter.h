#pragma once
#include "Network.h"
class IParameter {
public:
	virtual void operator()(const int param)=0;
	virtual void Call(const int param)=0;
};

template <class TParam> class Parameter : public IParameter {
private:
	void (TParam::*fpt)(const int);   // pointer to member function
	TParam* pt2Object;                  // pointer to object

public:
	Parameter(TParam* _pt2Object, void(TParam::*_fpt)(const int))
	{ pt2Object = _pt2Object;  fpt=_fpt; };
	virtual void operator()(const int param)
	{ (*pt2Object.*fpt)(param);};
	virtual void Call(const int param)
	{ (*pt2Object.*fpt)(param);};
};


class IResult {
public:
	virtual string operator()()=0;
	virtual string Call()=0;
};
template <class TParam> class Result : public IResult {
private:
	string (TParam::*fpt)();   // pointer to member function
	TParam* pt2Object;                  // pointer to object

public:
	Result(TParam* _pt2Object, std::string(TParam::*_fpt)())
	{ pt2Object = _pt2Object;  fpt=_fpt; };
	virtual string operator()()
	{ return (*pt2Object.*fpt)();};
	virtual string Call()
	{ return (*pt2Object.*fpt)();};
};