#pragma once
#include "Parameter.h"

class ParameterSet {
private:
	vector<IParameter*> _pSetFunctors;
	vector<string> _pSetNames;
	vector< vector<int> > _pSetValues;
	vector<IResult*> _pGetFunctors;
	vector<int> _curIdx;
	vector<string> _results;
	string _fName;

public:
	ParameterSet(string fileName) {
		_fName=fileName;
	}

	void Reset();
	void AddParameters(string,IParameter*,vector<int>);
	void AddResultRecorder(IResult*);
	bool HasParameters();
	bool SetNext();
	void StoreResult();
	void WriteResult();
};