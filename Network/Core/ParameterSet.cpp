#include "ParameterSet.h"
void ParameterSet::Reset() {
	_pSetFunctors.clear();
	_pSetNames.clear();
	for(int i=0;i<_pSetValues.size();i++)
		_pSetValues[i].clear();
	_pSetValues.clear();
	_pGetFunctors.clear();
	_curIdx.clear();
	_fName="";
}

void ParameterSet::AddParameters(string pName, IParameter* par, std::vector<int> val) {
	_pSetNames.push_back(pName);
	_pSetFunctors.push_back(par);
	_pSetValues.push_back(val);
	_curIdx.push_back(0);
}

void ParameterSet::AddResultRecorder(IResult* res) {
	_pGetFunctors.push_back(res);
}

bool ParameterSet::HasParameters() {
	if(_curIdx.size()<1 || _pSetValues.size()<1)
		return false;
	return _curIdx[0]<_pSetValues[0].size();
}

bool ParameterSet::SetNext() {
	if(!HasParameters())
		return false;
	for(int i=0;i<_curIdx.size();i++) {
		ostringstream buff;
		_pSetFunctors[i]->Call(_pSetValues[i][_curIdx[i]]);
		buff<<_pSetNames[i] << ": " << _pSetValues[i][_curIdx[i]]<<endl;
		_results.push_back(buff.str());
	}
	_curIdx[_curIdx.size()-1]++;
	for(int i=_curIdx.size()-1;i>0;i--) {
		if(_curIdx[i]==_pSetValues[i].size()) {
			_curIdx[i]=0;
			_curIdx[i-1]+=1;
		}
	}
	return true;
}

void ParameterSet::StoreResult() {
	for(int i=0; i<_pGetFunctors.size(); i++) {
		_results.push_back(_pGetFunctors[i]->Call());
	}
	_results.push_back("--------------------------\n");
}

void ParameterSet::WriteResult() {
	FILE *f=fopen(_fName.c_str(), "w+");
	for(int i=0;i<_results.size();i++)
		fprintf(f,"%s",_results[i].c_str());
	fclose(f);
}